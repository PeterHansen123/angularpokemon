export interface Pokemon{
    id: string;
    count: number;
    next: string;
    previous: string;
    result: PokemonInterface[];       
}

export interface PokemonInterface{
    name: string;
    url: string;
}
