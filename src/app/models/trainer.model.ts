import { Pokemon } from "./pokemon.model";

export interface Trainer {
    id: string;
    username: string;
    pokemon: Pokemon[];
    //favourites: string[];   
}

