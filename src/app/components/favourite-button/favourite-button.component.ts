import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { FavouriteService } from 'src/app/services/favourite.service'; 
import { TrainerService } from 'src/app/services/trainer.service';

/*
export class Agent{
  ID:string | undefined;
}

export let agents : Agent[] = []; */

@Component({
  selector: 'app-favourite-button',
  templateUrl: 'favourite-button.component.html',  
  styleUrls: ['./favourite-button.component.css']
})

export class FavouriteButtonComponent implements OnInit {

  public isFavourite: boolean = false;
  public clicked: boolean = false;
  
  @Input() pokemonId: string = "";

  get loading() : boolean {
    return this.favouriteService.loading
  }

  constructor(
    private trainerService: TrainerService,
    private readonly favouriteService : FavouriteService
  ) { }

  ngOnInit(): void {
   this.isFavourite = this.trainerService.inFavourites(this.pokemonId)
   this.clicked = Boolean(this.isFavourite); //contiune from here
  }
  
  onFavouriteClick(): void {
    //Add pokemons to the favourties
    this.clicked = true;
    alert("Clicked a favourite: " + this.pokemonId);
    this.favouriteService.addToFavourites(this.pokemonId)
      .subscribe({
        next: (response: Trainer) => {
          this.isFavourite = !this.trainerService.inFavourites(this.pokemonId);
        },
        error: (error: HttpErrorResponse) => {
        }
      })
  }

} 


/*
onFavouriteClick(event: any): void {
  //Add pokemons to the favourties
  alert("Clicked a favourite: " + this.pokemonId);
  this.favouriteService.addToFavourites(this.pokemonId)
    .subscribe({
      next: (response: Trainer) => {
        console.log("NEXT", response.id)
        event.target.disabled = true;
      },
      error: (error: HttpErrorResponse) => {
        console.log("ERROR", error.message);
      }
    })
} */