import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
const { apiPokemons } = environment;

@Injectable({
  providedIn: 'root'
})
//A service: a place where you put the calls to the server and which can be reused.
export class PokemonCatalogueService {

  private _pokemons: Pokemon[] = [];
  private _error: string = "";
  private _loading: boolean = false;

  public get pokemons(): Pokemon[] {
    return this._pokemons;
  }

  
  public get error() : string {
    return this._error;
  }

  
  public get loading() : boolean {
    return this._loading;
  }
  

  /*Java:
  * private HttpClient http;
  * 
  * Public PokemonCatalogueService(HttpClient http){
  * };
  *
  */
  constructor(private readonly http: HttpClient) { }

  public findAllPokemons(): void {
    this._loading = true;
    this.http.get<Pokemon[]>(apiPokemons)
    .pipe(
      finalize(() => {
        this._loading = false;
      })
    )
    .subscribe({
      next: (pokemons: Pokemon[]) => {
        this._pokemons = pokemons;

      },
      error: (error: HttpErrorResponse) => {
        this._error = error.message;
      }
    })
  }

  public pokemonById(id: string): Pokemon | undefined {
    return this._pokemons.find((pokemon: Pokemon) => pokemon.id === id);
  }

}