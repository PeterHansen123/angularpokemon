import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage.util';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {

  private _trainer: Trainer | undefined; //Alternative syntax instead of undefined: _trainer?: Trainer;
  
  
  public get trainer() : Trainer | undefined {    
    return this._trainer;
  }

  
  public set trainer(Trainer : Trainer | undefined) {
    StorageUtil.storageSave<Trainer>(StorageKeys.Trainer, Trainer!) //The exclamation mark says: aTrainer will never be undefined...
    this._trainer = Trainer;
  }

  constructor() {
    this._trainer = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer); 
    
   }

   public inFavourites(pokemonId: string): boolean {
    if (this._trainer) {
      return Boolean(this._trainer.pokemon.find((pokemon: Pokemon) => pokemon.id === pokemonId))
    } 
      return false;
   } 


   public addToFavourites(pokemonId: any): void {
    if (this._trainer) {
      this._trainer.pokemon.push(pokemonId);
    }
   }

   public removeFromFavourites(pokemonId: string): void {
    if (this._trainer) {
      this._trainer.pokemon = this._trainer.pokemon.filter((pokemon: Pokemon) => pokemon.id !== pokemonId)
    } 
   } 
} 

