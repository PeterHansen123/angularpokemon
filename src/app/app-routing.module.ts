import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./guards/auth.guard";
import { LandingPagePage } from "./pages/landing-page/landing-page.page";
import { PokemonCataloguePage } from "./pages/pokemon-catalogue/pokemon-catalogue.page";
import { TrainerPage } from "./pages/trainer/trainer.page";

const routes: Routes = [
    {
        path: "", //When the path is empty....
        pathMatch: "full", //This checks if the path is an empty string. If it is empty then...
        redirectTo: "/login" //....redirect to /login (landingpage), so that the first thing the user meets is the login page...       
    },
    {
        path: "login",
        component: LandingPagePage
    },
    {
        path: "pokemons",
        component: PokemonCataloguePage,
        canActivate: [ AuthGuard ]
    },
    {
        path: "trainer",
        component: TrainerPage,
        canActivate: [ AuthGuard ]
    }

]

//Just a description...
@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ], //Import a module
    exports: [
        RouterModule
    ] //Expose a module and its features

})
export class AppRoutingModule {

}